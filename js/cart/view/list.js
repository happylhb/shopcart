define([
    'underscore',
    'backbone',
    'template',
    '../collection/goods',
    '../model/goods',
    '../view/seller',
    'text!../tmpl/list.html'
],function(_,Backbone,Template,GoodsCollection,GoodsModel,SellerView,ListTmpl){
    var ListView = Backbone.View.extend({
        seller_infos_url:'http://test.qbzy.com/public/index/seller/infos/callback/define/ids/',
        seller_view:{},
        el:'#cart-list',
        collection:new GoodsCollection(),
        checked_list:new GoodsCollection(),
        change_checked_flag:0,//选中延时标示

        template:Template.compile(ListTmpl),
        initialize:function(model,option){

            this.load_data();

           /*this.collection.on('all',function (name,item) {
               console.log(name,item);
            });*/
            _.bindAll(this,'change_checked');
            this.collection.on('change:checked change:quantity remove',this.change_checked);

            this.render();
        },
        /*加载缓存中的数据*/
        load_data:function () {
            var self = this;
            var data =[
                {name:'商品1',price:90,quantity:10,total:0,seller_id:1},
                {name:'商品2',price:80,quantity:11,total:0,seller_id:2},
                {name:'商品3',price:60,quantity:12,total:0,seller_id:3},
                {name:'商品4',price:40,quantity:13,total:0,seller_id:2},
            ];
            _(data).each(function (item) {
               var itemModel = new GoodsModel();
                itemModel.set(item);
                self.collection.add(itemModel);
            });


        },
        //事件绑定
        events:{
            'click #all':'check_all',
            'click #settlement':'settlement'
        },
        /*全选按钮事件*/
        check_all:function () {
            var checked = this.$('#all').prop('checked');
            _.each(this.seller_view,function (view,key) {
                view.$('.seller-all').prop('checked',!checked).trigger('click');
            });
        },
        /*改变选择项时触发事件,统计总价*/
        change_checked:function () {
            var self = this;
            if(self.change_checked_flag>0){
                return;
            }

            self.change_checked_flag = setTimeout(function () {
                //延时执行改变事件
                var totals,
                    total = 0,
                    checked_models = self.collection.where({checked:true});
                self.checked_list.reset(checked_models);
                totals = self.checked_list.pluck('total');
                if(totals.length>0){
                    total = _.reduce(totals,function(memo, num){ return memo + num; });
                }
                self.$('#total').html(total);
                self.change_checked_flag = 0;
            },100);
        },
        /*结算*/
        settlement:function () {
            var settlement_data = this.checked_list.toJSON(),
                group_data = _.groupBy(settlement_data,'seller_id');
            //触发结算事件
            this.trigger('settlement',group_data);
        },
        //输出渲染
        render: function () {
            var seller_view,
                self = this,
                dataset = this.collection.models,
                group_set = _.groupBy(dataset,function (val) {
                    return val.attributes.seller_id;
                }),
                seller_ids = _.keys(group_set);
            //jsonp模式获取商家信息
            require([this.seller_infos_url+seller_ids.join(',')],function (seller_infos) {
                self.$el.html(self.template());
                _.each(group_set,function (group,key) {
                    seller_view= new SellerView({
                        dataset:group,
                        seller_info: seller_infos[key]
                    });

                    self.seller_view[key] = seller_view;
                    self.$('#list').append(seller_view.render().el);
                });
            });
        },
        unrender:function () {
            this.$el.remove();
        }
    });
    return ListView;
});