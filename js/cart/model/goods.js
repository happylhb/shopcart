define([
    'underscore',
    'backbone'
],function(_,Backbone){
    var GoodsModel = Backbone.Model.extend({
        //默认值
        defaults:{
            price:0,
            quantity:0,
            total:0,
            checked:false
        },
        initialize: function(){
            //改变数量时，重新计算价格
            this.bind("change:quantity",function(){
                var quantity = this.get("quantity"),
                    price = this.get('price');

                this.set('total',price*quantity);

            });
            this.bind("invalid",function(model,error){
                alert(error);
            });
        },
        validate:function(attributes){
            if(attributes.quantum < 1) {
                return "购买数量不能小于1";
            }
        },
        toggle:function () {
            this.set("checked", !this.get("checked"));
        }
    });
    return GoodsModel;
});