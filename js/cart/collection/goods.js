define([
    'underscore',
    'backbone',
    '../model/goods',
    'template'
],function(_,Backbone,GoodsModel,Template){
    var GoodsCollection = Backbone.Collection.extend({
        model:GoodsModel
    });
    return GoodsCollection;
});