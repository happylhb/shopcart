define(['jquery','underscore','./cart/view/list'], function ($, _,ListView) {
    var Controller = {
        index: function () {
            var listView = new ListView();
            //绑定事件
            listView.on('settlement',this.api.submit);
        },
        api:{
            /*添加商品到购物车*/
            add:function () {

            },
            /*移除商品*/
            remove:function () {

            },
            /*获取服务器上商品数据信息*/
            getGoods:function () {

            },
            //加载购物车中的数据
            load:function () {

            },
            /*提交商品*/
            submit:function (data) {
                //todo 向服务器提交订单信息
                console.log(data);
            }
        }
    };

    return Controller;
});


