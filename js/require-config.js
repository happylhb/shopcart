require.config({
    paths: {
        // 以下的包从bower的libs目录加载
        'jquery': './libs/jquery/dist/jquery.min',
        'template': './libs/art-template/lib/template-web',
        'text':'./libs/text/text',
        'underscore':'./libs/underscore/underscore-min',
        'backbone':'./libs/backbone/backbone-min'
    },
    // shim依赖配置
    shim: {

    },
    baseUrl: 'js/', //资源基础路径
    map: {

    },
    waitSeconds: 30,
    charset: 'utf-8' // 文件编码
});

